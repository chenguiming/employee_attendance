package com.xiaohao.employee_attendance;

import com.xiaohao.employee_attendance.bean.Department;
import com.xiaohao.employee_attendance.bean.User;
import com.xiaohao.employee_attendance.controller.UserController;
import com.xiaohao.employee_attendance.service.DeptService;
import com.xiaohao.employee_attendance.service.ReportSearchService;
import com.xiaohao.employee_attendance.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeAttendanceApplicationTests {

    @Autowired
    UserService userService;
    @Autowired
    UserController userController;
    @Autowired
    DeptService deptService;


    @Autowired
    ReportSearchService reportSearchService;

    @Test
    public void contextLoads() {
        User userByName = userService.findUserByAccount("1");
        System.out.println(userByName);
    }

    @Test
    public void useByPage() {
        Page<User> useByPage = userController.getUseByPage(0);
        System.out.println(useByPage.getContent());
    }


    @Test
    public void findUserByDept() {
        List<User> userByDept = userController.findUserByNameAndDept("", "1");
        System.out.println(userByDept);
    }

    @Test
    public void userUpdate() {

//        User user = new User();
//        user.setId(3);
//        user.setMobile("17862971078");
//        userService.saveUser(user);
    }

    @Test
    public void userDelete() {

        deptService.getByName("2");
    }


    @Test
    public void deptFind() {

//        userService.deleteUser("2");
    }

    @Test
    public void deptfindById() {
        Department byId = deptService.findById(2);
        System.out.println(byId);

    }

    @Test
    public void updateDept() {

//        userService.deleteUser("9");

    }


    @Test
    public void reportTest() {


        System.out.println(reportSearchService.getByNameAndDate("1", "2019-11-11", "2019-12-13"));
    }


}
