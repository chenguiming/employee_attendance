package com.xiaohao.employee_attendance.controller;

import com.xiaohao.employee_attendance.bean.ReportRecord;
import com.xiaohao.employee_attendance.bean.User;
import com.xiaohao.employee_attendance.service.ReportSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-12 07:53
 * @package com.xiaohao.employee_attendance.controller
 * @description
 */
@Controller
public class ReportController {
    @Autowired
    ReportSearchService reportSearchService;

    @RequestMapping("report/reportSearch")
    public String deptSearch(@RequestParam(defaultValue = "0") int page, Model model) {

        Page<ReportRecord> byPage = reportSearchService.getByPage(page, 5);
        model.addAttribute("reports", byPage);
        return "report/reportSearch";

    }


    @RequestMapping("report/reportSearchByNameAndDate")
    @ResponseBody
    public List<ReportRecord> deptSearch(@RequestParam(defaultValue = "") String account,
                                         @RequestParam(defaultValue = "") String beginDate,
                                         @RequestParam(defaultValue = "") String endDate) {
        System.out.println(account);
        System.out.println(beginDate);
        System.out.println(endDate);
        return reportSearchService.getByNameAndDate(account, beginDate, endDate);

    }

    @RequestMapping("report/reportUpdate")
    public String deptUpdate(int id, Model model) {
        System.out.println(id);
        ReportRecord byId = reportSearchService.findById(id);
        model.addAttribute("report", byId);
        return "report/reportUpdate";

    }


    @RequestMapping("report/reportSave")
    public String reportSave(ReportRecord reportRecord) {
        reportSearchService.reportSave(reportRecord);
        return "forward:reportSearch";

    }


    @RequestMapping("report/reportDelete")
    @ResponseBody
    public void deptDelete(int id) {
        reportSearchService.reportDeleteById(id);
    }

    @RequestMapping("report/reportInsert")
    public String reportInsert(ReportRecord reportRecord, HttpSession httpSession) {
        User user = (User) httpSession.getAttribute("user");
        reportSearchService.reportSave(reportRecord);
        return "forward:reportSearch";
    }

}
