package com.xiaohao.employee_attendance.config;

import com.xiaohao.employee_attendance.interceptor.AutoLoginIntercepetor;
import com.xiaohao.employee_attendance.interceptor.CheckLoginInterceptor;
import com.xiaohao.employee_attendance.interceptor.RemeberIntercepetor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-04 14:42
 * @package com.xiaohao.employee_attendance.config
 * @description
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    AutoLoginIntercepetor autoLoginIntercepetor;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/index").setViewName("index");
        registry.addViewController("/").setViewName("login");
        registry.addViewController("/nav").setViewName("nav");
        registry.addViewController("/main").setViewName("main");
        registry.addViewController("/dept/deptUpdate").setViewName("dept/deptUpdate");
        registry.addViewController("/dept/deptInsertView").setViewName("dept/deptInsert");
        registry.addViewController("/mydesktop/mydesktop").setViewName("mydesktop/mydesktop");
        registry.addViewController("/report/reportUpdate").setViewName("report/reportUpdate");
        registry.addViewController("/report/reportInsertView")
                .setViewName("report/reportInsert");
        registry.addViewController("/worksyouninn/worksyouninnSearch")
                .setViewName("worksyouninn/worksyouninnSearch");


        registry.addViewController("/restsyouninn/restsyouninnSearch")
                .setViewName("restsyouninn/restsyouninnSearch");


    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CheckLoginInterceptor()).addPathPatterns("/*").excludePathPatterns("/*login");
        registry.addInterceptor(new RemeberIntercepetor()).addPathPatterns("/user-login");
//        registry.addInterceptor(autoLoginIntercepetor).addPathPatterns("/login");
    }
}
