package com.xiaohao.employee_attendance.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-07 18:58
 * @package com.xiaohao.employee_attendance.bean
 * @description
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_rest_record")
public class RestRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer restId;
    @Column(nullable = false)
    String account;
    @Column(nullable = false)
    String restStartDate;
    @Column(nullable = false)
    String startTime;
    @Column(nullable = false)
    String restEndDate;
    @Column(nullable = false)
    String endTime;
    @Column(nullable = false)
    String restTime;
    @Column(nullable = false)
    String restCause;
    @Column(nullable = false)
    String biekao;
    @Column(nullable = false)
    int state;

}
