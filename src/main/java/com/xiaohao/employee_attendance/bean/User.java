package com.xiaohao.employee_attendance.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-04 14:22
 * @package com.xiaohao.employee_attendance.bean
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_user_info")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String account;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String departmentId;
    @Column(nullable = false)
    private int sex;
    @Column(nullable = true)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+0")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
    @Column(nullable = false)
    private String mobile;
    @Column(nullable = true)
    private String email;
    @Column(nullable = false, columnDefinition = "int default 0")
    private Integer userType;
    @Column(nullable = false, columnDefinition = "int default 0")
    private Integer mylevel;
    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDate createTime;
    @Column(nullable = false, columnDefinition = "int default 0")
    private int state;
}
