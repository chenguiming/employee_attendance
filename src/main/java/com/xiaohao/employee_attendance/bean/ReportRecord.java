package com.xiaohao.employee_attendance.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-07 19:06
 * @package com.xiaohao.employee_attendance.bean
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_report_record")
public class ReportRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer reportId;
    @Column(nullable = false)
    String account;
    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+0")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate reportDate;
    @Column(nullable = false)
    String workProcess;
    @Column(nullable = false)
    String workContent;
    @Column(nullable = false)
    String tomorrowPlan;
    @Column(nullable = true)
    String problem;
    @Column(nullable = true)
    String other;
}
