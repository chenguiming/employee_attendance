package com.xiaohao.employee_attendance.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-07 19:08
 * @package com.xiaohao.employee_attendance.bean
 * @description
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_department")
public class Department {
    @Id
    Integer departmentId;
    @Column(nullable = false)
    String departmentName;
    @Column(nullable = true)
    String manager;
    @Column(nullable = false, columnDefinition = "int default 0")
    Integer totalUser;
    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+0")
    LocalDate createTime;

}
