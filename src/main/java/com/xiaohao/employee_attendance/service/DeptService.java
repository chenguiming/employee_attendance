package com.xiaohao.employee_attendance.service;

import com.xiaohao.employee_attendance.bean.Department;
import com.xiaohao.employee_attendance.dao.DeptDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-08 07:38
 * @package com.xiaohao.employee_attendance.service
 * @description
 */
@Service
public class DeptService {
    @Autowired
    DeptDao deptDao;


    public List<Department> findAllDept() {
        return deptDao.findAll();
    }


    public Page<Department> getByPage(int page, int size) {
        Page<Department> all = deptDao.findAll(PageRequest.of(page, size));
        return all;
    }

    public Department getByName(String name) {
        return deptDao.findByDepartmentName(name);
    }

    public Department findById(Integer id) {
        Optional<Department> byId = deptDao.findById(id);

        return byId.orElse(null);
    }


    public void deptSave(Department department) {
        deptDao.save(department);
    }

    public void deptDeleteById(int id) {
        deptDao.deleteById(id);
    }

    @Transactional
    public void updateTotalNumber(String name, int number) {
        System.out.println(name + "     " + number);
        deptDao.updateTotalNumberByName(name, number);
    }


}
