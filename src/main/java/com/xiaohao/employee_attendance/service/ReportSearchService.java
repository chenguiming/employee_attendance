package com.xiaohao.employee_attendance.service;

import com.xiaohao.employee_attendance.bean.ReportRecord;
import com.xiaohao.employee_attendance.dao.ReportSearchDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-12 07:57
 * @package com.xiaohao.employee_attendance.service
 * @description
 */
@Service
public class ReportSearchService {
    @Autowired
    ReportSearchDao reportSearchDao;

    public List<ReportRecord> findAllReport() {
        return reportSearchDao.findAll();
    }


    public Page<ReportRecord> getByPage(int page, int size) {
        Page<ReportRecord> all = reportSearchDao.findAll(PageRequest.of(page, size));
        return all;
    }

    public List<ReportRecord> getByNameAndDate(String name, String beginDate, String endTime) {
        if (name.isEmpty() && !beginDate.isEmpty()) {
            return reportSearchDao.getByDate(beginDate, endTime);
        }
        if (!name.isEmpty() && beginDate.isEmpty()) {
            return reportSearchDao.findByAccount(name);
        }

        return reportSearchDao.getByNameAndDate(name, beginDate, endTime);

    }

    public ReportRecord findById(Integer id) {
        Optional<ReportRecord> byId = reportSearchDao.findById(id);

        return byId.orElse(null);
    }


    public void reportSave(ReportRecord ReportSearch) {
        reportSearchDao.save(ReportSearch);
    }

    public void reportDeleteById(int id) {
        reportSearchDao.deleteById(id);
    }
}
