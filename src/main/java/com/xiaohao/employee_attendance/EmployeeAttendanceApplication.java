package com.xiaohao.employee_attendance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 小浩
 */
@SpringBootApplication
public class EmployeeAttendanceApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmployeeAttendanceApplication.class, args);
    }

}
