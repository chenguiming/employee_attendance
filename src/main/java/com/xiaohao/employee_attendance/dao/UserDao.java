package com.xiaohao.employee_attendance.dao;

import com.xiaohao.employee_attendance.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-04 15:32
 * @package com.xiaohao.employee_attendance.dao
 * @description
 */
public interface UserDao extends JpaRepository<User, Integer> {
    User findUserByAccount(String account);

    List<User> findUserByNameAndDepartmentId(String name, String dept);

    List<User> findUserByName(String Name);

    List<User> findUserByDepartmentId(String dept);

    int deleteUserByAccount(String account);
}