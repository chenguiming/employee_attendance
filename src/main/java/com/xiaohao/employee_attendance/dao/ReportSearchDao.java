package com.xiaohao.employee_attendance.dao;

import com.xiaohao.employee_attendance.bean.ReportRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-12 07:57
 * @package com.xiaohao.employee_attendance.dao
 * @description
 */

public interface ReportSearchDao extends JpaRepository<ReportRecord, Integer> {
    List<ReportRecord> findByAccount(String sccount);

    @Query(nativeQuery = true, value = "select * from t_report_record where report_date between :beginDate and :endDate")
    List<ReportRecord> getByDate(@Param("beginDate") String beginDate, @Param("endDate") String endDate);


    @Query(nativeQuery = true, value = "select * from t_report_record where account like %:account% and  report_date between :beginDate and :endDate")
    List<ReportRecord> getByNameAndDate(@Param("account") String account, @Param("beginDate") String beginDate, @Param("endDate") String endDate);


}
