package com.xiaohao.employee_attendance.dao;

import com.xiaohao.employee_attendance.bean.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-08 07:38
 * @package com.xiaohao.employee_attendance.dao
 * @description
 */

public interface DeptDao extends JpaRepository<Department, Integer> {
    Department findByDepartmentName(String name);


    @Modifying
    @Query(nativeQuery = true, value =
            "UPDATE t_department,t_user_info " +
                    "SET total_user = total_user+:num " +
                    "WHERE " +
                    "department_name = t_user_info.department_id " +
                    "AND t_user_info.account = :account ")
    int updateTotalNumber(@Param("account") String account, @Param("num") int num);

    @Modifying
    @Query("update Department set totalUser = totalUser + :num where departmentName = :name")
    int updateTotalNumberByName(@Param("name") String name, @Param("num") int num);

}
