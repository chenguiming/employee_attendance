package com.xiaohao.employee_attendance.dao;

import com.xiaohao.employee_attendance.bean.WorkRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created with IntelliJ IDEA.
 *
 * @author 小浩
 * @date 2019-03-12 22:13
 * @package com.xiaohao.employee_attendance.dao
 * @description
 */
public interface WorkRecordDao extends JpaRepository<WorkRecord, Integer> {
}
